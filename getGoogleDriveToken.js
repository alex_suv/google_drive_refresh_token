'use strict';

let fs = require('fs');
let path = require('path');
let readline = require('readline');
let google = require('googleapis');
let OAuth2Client = google.auth.OAuth2;

// Client ID and client secret are available at
// https://code.google.com/apis/console
const client_secret = {
	"installed": {
		"client_id": "***",
		"auth_uri": "https://accounts.google.com/o/oauth2/auth",
		"token_uri": "https://accounts.google.com/o/oauth2/token",
		"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
		"client_secret": "***",
		"redirect_uris": [
			"urn:ietf:wg:oauth:2.0:oob",
			"http://localhost"
		]
	}
};

const CLIENT_ID = client_secret.installed.client_id;
const CLIENT_SECRET = client_secret.installed.client_secret;
const REDIRECT_URL = client_secret.installed.redirect_uris[0];

const TOKEN_DIR = path.join(__dirname, 'googleDriveToken');
const TOKEN_PATH = path.join(TOKEN_DIR, 'googleDriveToken.json');

let oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

let rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

function getToken(oauth2Client) {
	// generate consent page url
	var url = oauth2Client.generateAuthUrl({
		access_type: 'offline', // will return a refresh token
		scope: 'https://www.googleapis.com/auth/drive.readonly' // request readonly permissions
	});

	console.log('Visit the url: ', url);
	rl.question('Enter the code here:', function(code) {
		// request access token
		oauth2Client.getToken(code, function(err, tokens) {
			// set tokens to the client
			console.log(tokens);
			storeToken(tokens);
		});
	});
}

function storeToken(token) {
	try {
		fs.mkdirSync(TOKEN_DIR);
	} catch (err) {
		if (err.code !== 'EXIST') {
			throw err;
		}
	}
	fs.writeFile(TOKEN_PATH, JSON.stringify(token));
	console.log('Token stored to ' + TOKEN_PATH);
}

getToken(oauth2Client);
